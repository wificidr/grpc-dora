use domain::base::{
    MessageBuilder, Rtype, StaticCompressor, StreamTarget, Question, Message, ParsedDname
};
use domain::rdata::A;
use tonic::{transport::Server, Request, Response, Status};

use grpc_dora::dns_service_server::{DnsService, DnsServiceServer};
use grpc_dora::DnsPacket;

pub mod grpc_dora {
tonic::include_proto!("coredns.dns");
}

#[derive(Debug, Default)]
pub struct MyDnsService {}

#[tonic::async_trait]
impl DnsService for MyDnsService {
    async fn query(
        &self,
        request: Request<DnsPacket>,
    ) -> Result<Response<DnsPacket>, Status> {
        println!("Got a request: {:?}", request);
        let dns_msg = Message::from_octets(request.into_inner().msg).unwrap();

        let mut resp: Vec<Vec<u8>> = Vec::new();
        for question in dns_msg.question() {
            if let Ok(q) = question {
                // Do something with the record ...
                println!("Got a question! {:?}", q);
                match q.qtype() {
                    Rtype::Ptr => {
                        println!("It's a pointer!");
                        let bslice = answer_a(q).as_dgram_slice().to_vec();
                        resp.push(bslice);
                    },
                    Rtype::A => {
                        println!("It's an A!");
                    },
                    _ => println!("WTF, Chuck?!"),
                };
            }
        }

        let reply = grpc_dora::DnsPacket {
            msg: resp.pop().unwrap()
        };

        Ok(Response::new(reply))
    }
}

fn answer_a(q: Question<ParsedDname<&Vec<u8>>>) -> StreamTarget<Vec<u8>> {
    // Create a message builder wrapping a compressor wrapping a stream
    // target.
    let mut msg = MessageBuilder::from_target(
        StaticCompressor::new(
            StreamTarget::new_vec()
        )
    ).unwrap();

    // Set the RD bit in the header and proceed to the question section.
    msg.header_mut().set_rd(true);
    let mut msg = msg.question();

    // Add a question and proceed to the answer section.
    msg.push((&q.qname(), Rtype::A)).unwrap();
    let mut msg = msg.answer();

    // Add two answer and proceed to the additional sections
    msg.push((&q.qname(), 86400, A::from_octets(192, 0, 2, 1))).unwrap();
    msg.push((&q.qname(), 86400, A::from_octets(192, 0, 2, 2))).unwrap();
    let mut msg = msg.additional();

    // Add an OPT record.
    msg.opt(|opt| {
        opt.set_udp_payload_size(4096);
        Ok(())
    }).unwrap();

    // Convert the builder into the actual message.
    let target = msg.finish().into_target();

    // A stream target can provide access to the data with or without the
    // length counter:
    // let _ = target.as_stream_slice(); // With length
    target
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:50051".parse()?;
    let service = MyDnsService::default();

    Server::builder()
        .add_service(DnsServiceServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}

